#!/bin/sh

# define app directory
CONFIG=/home/defaults
APP=/home/app

# ADD ./config/nginx.conf /etc/nginx/
echo "copying nginx.conf"
cp $CONFIG/nginx.conf /etc/nginx/ 

# ADD ./config/php-fpm.conf /etc/php/
echo "copying config php-fpm.conf"
cp $CONFIG/php-fpm.conf /etc/php/

# ADD ./config/php.ini /etc/php/
echo "copying config php.ini"
cp $CONFIG/php.ini /etc/php/

# remove .git folder
rm -fr /home/app/.git

# start php-fpm
mkdir -p /home/logs/php-fpm

# start nginx
mkdir -p /home/logs/nginx
mkdir -p /tmp/nginx
chown nginx /tmp/nginx

# fix permissions
chmod -R 775 /home/app
chmod -R 777 /home/logs
chown -R www-data:www-data /home/app/assets /home/app/application/assets
chown nginx /home/logs

echo "all services started!"

# Start supervisord and services
/usr/bin/supervisord -n -c /etc/supervisord.conf
